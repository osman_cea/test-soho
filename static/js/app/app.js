// app
var testApp = angular.module('testApp', ['ngAnimate', 'ngTouch', 'ui.bootstrap']);

testApp.controller('testController', [function() {
	// Controller as test
	var vm = this;

    vm.steps = 'step-1';

    vm.removeRow = function(index) {
        vm.recipients.splice(index, 1);
    };

    vm.getTotal = function() {
        var total = 0;

        vm.recipients.forEach(function(rec, index){
            var mstr = rec.transaction.ammount.split('.');
            var mint = "";

            for ( var i = 0; i < mstr.length; i++ ) {
                mint += mstr[i];
            }

            total += parseInt(mint);
        });

        return total;
    }

    // Transfers
    vm.transfers = [
    	{
    		title: "Transferencias Online",
    		templateUrl: 'templates/content.html'
    	},
    	{
    		title: "Transferencias Alto Valor",
    		templateUrl: 'templates/content.html'
    	},
    	{
    		title: "DVP",
    		templateUrl: 'templates/content.html'
    	},
    	{
    		title: "Pago Línea de Crédito",
    		templateUrl: 'templates/content.html'
    	},
    	{
    		title: "Consultas y Cartola de Transacciones Realizadas",
    		templateUrl: 'templates/content.html'
    	}
    ];

    // Origin accounts
    vm.origins = [
    	{
    		alias: "Alias cuenta 1",
    		number: "123456789123456"
    	},
    	{
    		alias: "Alias cuenta 2",
    		number: "231234567891456"
    	}
    ]

    // Transfer recipients 
    vm.recipients = [
    	{
    		firstName: "Ricardo",
    		middleName: "",
    		lastName: "Fellini",
    		id: "12.553.778-9",
    		bank: {
    			name: "Banco de crédito e inversiones",
    			account: "3504-5486-548756548921"
    		},
    		transaction: {
    			alias: "Transferencia",
    			description: "Alto Valor",
    			ammount: "200.000.000.000"
    		}
    	},
    	{
    		firstName: "Pedro",
    		middleName: "",
    		lastName: "Rodríguez",
    		id: "21.11.777-9",
    		bank: {
    			name: "Scotiabank",
    			account: "0544512321579897"
    		},
    		transaction: {
    			alias: "DVP",
    			description: "RF1000",
    			ammount: "10.000.000"
    		}
    	},
    	{
    		firstName: "Juan",
    		middleName: "Pedro",
    		lastName: "Lopez",
    		id: "12.553.433-9",
    		bank: {
    			name: "Banco X",
    			account: "021112223334"
    		},
    		transaction: {
    			alias: "Transferencia",
    			description: "Alto Valor",
    			ammount: "10.000.000"
    		}
    	},
    	{
    		firstName: "María",
    		middleName: "",
    		lastName: "Gutierrez",
    		id: "14.11.777-K",
    		bank: {
    			name: "Banco de la esquina",
    			account: "111444222111"
    		},
    		transaction: {
    			alias: "Transferencia",
    			description: "Alto Valor",
    			ammount: "10.000.000"
    		}
    	},
    	{
    		firstName: "Mario",
    		middleName: "Felipe",
    		lastName: "Hernández",
    		id: "14.222.333-4",
    		bank: {
    			name: "Banco del Mutuo Soccorso",
    			account: "11144478466654"
    		},
    		transaction: {
    			alias: "DVP",
    			description: "RF2003",
    			ammount: "10.000.000"
    		}
    	},
    	{
    		firstName: "María",
    		middleName: "Filipa",
    		lastName: "Fernández",
    		id: "14.222.333-4",
    		bank: {
    			name: "Banco del Mutuo Soccorso",
    			account: "11144478466654"
    		},
    		transaction: {
    			alias: "DVP",
    			description: "RF2003",
    			ammount: "10.000.000"
    		}
    	},
    	{
    		firstName: "Juan",
    		middleName: "Carlos",
    		lastName: "Bodoque",
    		id: "14.222.333-4",
    		bank: {
    			name: "Hipódromo de Chile",
    			account: "11144478466654"
    		},
    		transaction: {
    			alias: "Apuesta",
    			description: "Tormenta China",
    			ammount: "10.000.000"
    		}
    	}
    ];
}]);
